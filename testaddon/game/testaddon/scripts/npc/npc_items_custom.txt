"DOTAAbilities"
{

	"item_pickup_coin_1"
	{

		// General
		//-------------------------------------------------------------------------------------------------------------
		"ID"					        "1201"
		"BaseClass"						"item_datadriven"
		"AbilityBehavior"         		"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"Model"							"models/props_gameplay/halloween_candy.vmdl"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemQuality"					"rare"
		"ItemShareability"				"ITEM_FULLY_SHAREABLE"
		"ItemDeclarations"				"DECLARE_PURCHASES_TO_TEAMMATES | DECLARE_PURCHASES_TO_SPECTATORS | DECLARE_PURCHASES_IN_SPEECH"
		"ItemDisassembleRule"			"DOTA_ITEM_DISASSEMBLE_NEVER"
		"ItemCost"                      "0"
		"ItemDroppable"					"1"
		"ItemPurchasable"				"0"
		"ItemSellable"					"0"
		"ItemKillable"					"0"
		"AllowedInBackpack"				"0"
		"IsTempestDoubleClonable"		"0"
		"ItemCastOnPickup"				"1"


	}

	"item_pickup_coin_2"
	{

		// General
		//-------------------------------------------------------------------------------------------------------------
		"ID"					        "1202"
		"BaseClass"						"item_datadriven"
		"AbilityBehavior"         		"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"Model"							"models/props_gameplay/halloween_cupcakes001_bat.vmdl"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemQuality"					"rare"
		"ItemShareability"				"ITEM_FULLY_SHAREABLE"
		"ItemDeclarations"				"DECLARE_PURCHASES_TO_TEAMMATES | DECLARE_PURCHASES_TO_SPECTATORS | DECLARE_PURCHASES_IN_SPEECH"
		"ItemDisassembleRule"			"DOTA_ITEM_DISASSEMBLE_NEVER"
		"ItemCost"                      "0"
		"ItemDroppable"					"1"
		"ItemPurchasable"				"0"
		"ItemSellable"					"0"
		"ItemKillable"					"0"
		"AllowedInBackpack"				"0"
		"IsTempestDoubleClonable"		"0"
		"ItemCastOnPickup"				"1"


	}

	"item_pickup_coin_3"
	{

		// General
		//-------------------------------------------------------------------------------------------------------------
		"ID"					        "1203"
		"BaseClass"						"item_datadriven"
		"AbilityBehavior"         		"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"Model"							"models/props_gameplay/halloween_cupcakes001_spider.vmdl"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemQuality"					"rare"
		"ItemShareability"				"ITEM_FULLY_SHAREABLE"
		"ItemDeclarations"				"DECLARE_PURCHASES_TO_TEAMMATES | DECLARE_PURCHASES_TO_SPECTATORS | DECLARE_PURCHASES_IN_SPEECH"
		"ItemDisassembleRule"			"DOTA_ITEM_DISASSEMBLE_NEVER"
		"ItemCost"                      "0"
		"ItemDroppable"					"1"
		"ItemPurchasable"				"0"
		"ItemSellable"					"0"
		"ItemKillable"					"0"
		"AllowedInBackpack"				"0"
		"IsTempestDoubleClonable"		"0"
		"ItemCastOnPickup"				"1"


	}

	"item_inventory_coin"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"ID"							"1204"
		"BaseClass"						"item_lua"
		"ScriptFile"					"items/item_inventory_coin.lua"
		"AbilityTextureName"			"inventory_coin"
		"AbilityCastAnimation" 			"ACT_DOTA_ATTACK"
		//"Model"							"models/props_gameplay/recipe.vmdl"

		// Behavior
		//-------------------------------------------------------------------------------------------------------------
        "AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_FRIENDLY | DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_CREEP"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemDeclarations"				"DECLARE_PURCHASES_TO_TEAMMATES | DECLARE_PURCHASES_TO_SPECTATORS | DECLARE_PURCHASES_IN_SPEECH"
		"ItemShareability"				"ITEM_FULLY_SHAREABLE_STACKING"
		"ItemDisassembleRule"			"DOTA_ITEM_DISASSEMBLE_NEVER"
		"ItemQuality"					"artifact"
		"ItemCost"                      "1"
		"ItemDroppable"					"0"
		"ItemPurchasable"				"0"
		"ItemSellable"					"0"
		"ItemKillable"					"1"
		"ItemInitialCharges"			"1"
		"ItemStackable" 				"1"
		"ItemPermanent" 				"0"
		"SideShop" 						"0"
		"IsTempestDoubleClonable"		"0"
		"AllowedInBackpack"				"0"

	}

}