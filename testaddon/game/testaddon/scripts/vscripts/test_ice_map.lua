if test_ice_map == nil then
    _G.test_ice_map = class({})
end
---------------------------------ice modif------------------

LinkLuaModifier( "modifier_directional_move", "modifiers/modifier_directional_move", LUA_MODIFIER_MOTION_HORIZONTAL )
LinkLuaModifier( "modifier_glide_move", "modifiers/modifier_glide_move", LUA_MODIFIER_MOTION_HORIZONTAL )
LinkLuaModifier( "modifier_directional_move_order", "modifiers/modifier_directional_move_order", LUA_MODIFIER_MOTION_HORIZONTAL )

LinkLuaModifier( "modifier_move_order", "modifiers/modifier_move_order", LUA_MODIFIER_MOTION_HORIZONTAL )

LinkLuaModifier( "modifier_apply_fail_animation", "modifiers/modifier_apply_fail_animation", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_fail_animation", "modifiers/modifier_fail_animation", LUA_MODIFIER_MOTION_NONE )
----------------------------------------------------------

LinkLuaModifier( "modifier_drop_coin", "modifiers/modifier_drop_coin", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_coin_debuff", "modifiers/modifier_coin_debuff", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_health_debuff", "modifiers/modifier_health_debuff", LUA_MODIFIER_MOTION_NONE )



function test_ice_map:InitGameMode()
    print( "InitGameMode" )
    GameRules:SetCustomGameTeamMaxPlayers( DOTA_TEAM_GOODGUYS, 4 )
    GameRules:SetCustomGameTeamMaxPlayers( DOTA_TEAM_BADGUYS, 4 )

    GameRules:SetUseUniversalShopMode( true )
    GameRules:SetHeroSelectionTime( 30.0 )
    GameRules:SetStrategyTime( 0.0 )
    GameRules:SetShowcaseTime( 0.0 )
    GameRules:SetPreGameTime( 5.0 )

    GameRules:GetGameModeEntity():SetCustomGameForceHero('npc_dota_hero_juggernaut');

    ListenToGameEvent("entity_killed", Dynamic_Wrap(test_ice_map, "OnEntityKilled"), self)
    ListenToGameEvent('dota_item_picked_up', Dynamic_Wrap(test_ice_map, 'OnItemPickedUp'), self)
end

function ApplyIceModifier(data)
    print( "ApplyIceModifier" )
    local caster = data.activator
    caster:AddNewModifier( caster, nil, "modifier_move_order", {} )
    --caster:AddNewModifier( caster, nil, "modifier_directional_move_order", {} )
    --caster:AddNewModifier( caster, nil, "modifier_directional_move", {} )
    caster:AddNewModifier( caster, nil, "modifier_glide_move", {} )
    caster:AddNewModifier( caster, nil, "modifier_apply_fail_animation", {} )
    --caster:AddNewModifier( caster, nil, "modifier_fail_animation", {} )
end

function RemoveIceModifier(data)
    print( "RemoveIceModifier" )
    local caster = data.activator
    caster:RemoveModifierByName("modifier_move_order")
    --caster:RemoveModifierByName("modifier_directional_move_order")
    --caster:RemoveModifierByName("modifier_directional_move")
    caster:RemoveModifierByName("modifier_glide_move")
    caster:RemoveModifierByName("modifier_apply_fail_animation")
    --caster:RemoveModifierByName("modifier_fail_animation")
end


function test_ice_map:OnEntityKilled(data)
    local killedEntity = EntIndexToHScript(data.entindex_killed)
    --print("entity killed")

    if killedEntity:IsRealHero() then

        local item = nil
        local charges = 0
        for i = 0, 8 do
            item = killedEntity:GetItemInSlot(i)
            if item ~= nil and item:GetAbilityName() == "item_inventory_coin" then
                if item:IsStackable() == true then
                    charges = item:GetCurrentCharges()
                    for charges = item:GetCurrentCharges(), 1, -1 do
                        DropCoin(killedEntity:GetAbsOrigin())
                    end
                    killedEntity:RemoveItem(item)
                end
            end
        end
    end
end

function test_ice_map:OnItemPickedUp(data)
    local hItem = EntIndexToHScript( data.ItemEntityIndex )
    local hHero = EntIndexToHScript( data.HeroEntityIndex )
    local itemName = data.itemname
    local newItem = nil
    local newItemName = "item_inventory_coin"
    local coinLimit = false


    if hHero and itemName then
        local isCoin = false
        for i = 1, 3 do
            local name = "item_pickup_coin_" .. i
            if itemName == name then
                isCoin = true
            end
        end

        if isCoin then
            if hHero:HasItemInInventory(newItemName) then
                newItem = FindItemInInventory(hHero,newItemName)
                if newItem then
                    if newItem:GetCurrentCharges() >= 10 then
                        coinLimit = true
                    else
                        newItem:SetCurrentCharges(newItem:GetCurrentCharges() + 1)
                    end
                end
            else
                hHero:AddItemByName(newItemName)
            end

            if coinLimit then
                DropCoin(hHero:GetAbsOrigin())
            else
                local modifier = hHero:AddNewModifier( hHero, nil, "modifier_coin_debuff", {} )
                modifier:IncrementStackCount()
                hHero:AddNewModifier( hHero, nil, "modifier_health_debuff", {} )
            end

            UTIL_Remove( hItem )
        end
    end
end

function FindItemInInventory(hTarget, itemName)
    local item = nil
    for i = 0, 5 do
        item = hTarget:GetItemInSlot(i)
        if item ~= nil then
            if item:GetAbilityName() == itemName then
                return item
            end
        end
    end
    return nil
end


function DropCoin(pos)
    local itemName = "item_pickup_coin_" .. RandomInt(1,3)
    local newItem = CreateItem(itemName, nil, nil)
    newItem:SetPurchaseTime(0)
    CreateItemOnPositionSync(pos, newItem)
    newItem:LaunchLoot(false, 300, 0.75, pos + RandomVector(RandomFloat(100, 300)))
end