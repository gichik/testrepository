if item_inventory_coin == nil then
	item_inventory_coin = class({})
end

function item_inventory_coin:CastFilterResultTarget(hTarget)
	--print("Error")
	if IsServer() then
		if hTarget:GetUnitName() == "npc_candy_box" then
			return UF_SUCCESS
		end
		return UF_FAIL_CUSTOM
	end
end


function item_inventory_coin:GetCustomCastErrorTarget(hTarget)
	--print("Error")
	if IsServer() then
		if hTarget:GetUnitName() == "npc_candy_box" then
			return UF_SUCCESS
		end
		return "#dota_hud_error_not_christmas_tree"
	end
end

function item_inventory_coin:OnSpellStart()
	print("OnSpellStart")
	local hCaster = self:GetCaster()
	local hTarget = self:GetCursorTarget()
	local hItem = self
	local itemName = self:GetAbilityName()

	if not hCaster:HasItemInInventory(itemName) then
		return
	end

	hItem = FindItemInInventory(hCaster,itemName)
	if not hItem then
		return
	end

	hTarget:EmitSound("Item.DropWorld")
	--print(hItem:GetCurrentCharges() - hItem:GetInitialCharges())
	hItem:SetCurrentCharges(hItem:GetCurrentCharges() - hItem:GetInitialCharges())

	if hItem:GetCurrentCharges() < hItem:GetInitialCharges() then
		hCaster:RemoveItem(hItem)
	end


	DecremStack(hCaster,hCaster:FindModifierByName("modifier_coin_debuff"))
	DecremStack(hCaster,hCaster:FindModifierByName("modifier_health_debuff"))

end

function FindItemInInventory(hTarget, itemName)
	local item = nil
	for i = 0, 5 do
		item = hTarget:GetItemInSlot(i)
		if item ~= nil then
			if item:GetAbilityName() == itemName then
				return item
			end
		end
	end
	return nil
end

function DecremStack(hTarget,hModifier)

	hModifier:DecrementStackCount()

	if hModifier:GetStackCount() < 1 then
		hTarget:RemoveModifierByName(hModifier:GetName())
	end

end