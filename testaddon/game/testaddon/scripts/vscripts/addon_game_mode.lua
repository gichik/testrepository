-- Generated from template

require( 'test_ice_map' )
require( 'timers' )


function Precache( context )
	--[[
		Precache things we know we'll use.  Possible file types include (but not limited to):
			PrecacheResource( "model", "*.vmdl", context )
			PrecacheResource( "soundfile", "*.vsndevts", context )
			PrecacheResource( "particle", "*.vpcf", context )
			PrecacheResource( "particle_folder", "particles/folder", context )
	]]
	PrecacheModel("models/props_gameplay/pumpkin_bucket.vmdl", context) --box
	PrecacheModel("models/props_gameplay/gold_coin001.vmdl", context) --trash


end

-- Create the game mode when we activate
function Activate()

	local MapName = GetMapName()
	print(MapName)

	if MapName == "test_ice_map" then
		print("----------------------------------------test_ice_map----------------------------------------")
		test_ice_map:InitGameMode()
	end
	
end