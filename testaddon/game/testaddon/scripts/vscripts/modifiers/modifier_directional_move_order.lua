modifier_directional_move_order = class({})

--------------------------------------------------------------------------------

function modifier_directional_move_order:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_directional_move_order:IsPurgable()
	return false
end

--------------------------------------------------------------------------------

function modifier_directional_move_order:OnCreated( kv )
	if IsServer() then
		local caster = self:GetParent()


        if not caster.currForward then
            caster.currForward = caster:GetForwardVector()
        end
        if not caster.newForward then
            caster.newForward = caster:GetForwardVector()
        end
        if not caster.speed then
            caster.speed = caster:GetBaseMoveSpeed()
        end
        --print(caster.updateSet)


		self.flag = 0

		if self:ApplyHorizontalMotionController() == false then 
			self:Destroy()
			return
		end

		--self:OnIntervalThink()
		--self:StartIntervalThink( 0.1 )

	end
end

--[[
function modifier_directional_move_order:OnIntervalThink()
	--self.oldForward = self.newForward

	--print(self.flag)
	--self.flag = self.flag - 1
	--if self.flag < 0 then
	--	self.flag = 0
	--end

	--if self.flag == 0 then
		self.newForward = self:GetParent():GetForwardVector()
		--self.currForward = self:GetParent():GetForwardVector()
	--end

end]]
--------------------------------------------------------------------------------

function modifier_directional_move_order:OnDestroy()
	if IsServer() then
		self:GetParent():RemoveHorizontalMotionController( self )
		self:GetParent():Interrupt()
	end
end


--------------------------------------------------------------------------------

function modifier_directional_move_order:UpdateHorizontalMotion( me, dt )
	if IsServer() then

		local caster = self:GetParent()
		--if (self.newForward.x + 0.2) > self.currForward.x and self.currForward.x > (self.newForward.x - 0.2)  then


		if caster.newForward == caster.currForward then
			caster.speed = caster.speed + 5
			
		else

			caster.speed = caster.speed - 5
		end

		if caster.speed <= 0 then
			caster.speed = 0
			caster.currForward = caster.newForward
			--print(self.currForward)
		end

		if caster.speed >= 400 then
			caster.speed = 400
		end


		local vNewPos = caster:GetOrigin() + ( caster.currForward * caster.speed * dt )
		if not GridNav:CanFindPath( caster:GetOrigin(), vNewPos ) then
			--self:Destroy()
			--return
			--self.flag = 10
			--self.currForward = self.currForward * (-1)

			caster.currForward = caster.currForward * (-1)
			vNewPos = caster:GetOrigin() + ( caster.currForward * caster.speed * dt )
		end
		me:SetOrigin( vNewPos )

	end
end

--------------------------------------------------------------------------------

function modifier_directional_move_order:OnHorizontalMotionInterrupted()
	if IsServer() then
		self:Destroy()
	end
end

--------------------------------------------------------------------------------

function modifier_directional_move_order:DeclareFunctions()
	local funcs = 
	{
		MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS,
		MODIFIER_PROPERTY_TURN_RATE_PERCENTAGE,
		MODIFIER_EVENT_ON_ORDER
	}
	return funcs
end

--------------------------------------------------------------------------------

function modifier_directional_move_order:OnOrder(data)
    if not IsServer() then


    end
	local caster = self:GetParent()
    local forward = ( data.new_pos -  caster:GetOrigin() ):Normalized()
	--caster:SetForwardVector(forward)
	caster.newForward = forward
    --print(self.currForward)
    --print( ( data.new_pos - self:GetParent():GetOrigin() ):Normalized() )
end

function modifier_directional_move_order:GetModifierTurnRate_Percentage( params )
	return 0
end


