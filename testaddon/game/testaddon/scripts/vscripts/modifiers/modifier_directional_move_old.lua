modifier_directional_move = class({})

--------------------------------------------------------------------------------

function modifier_directional_move:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_directional_move:IsPurgable()
	return false
end

--------------------------------------------------------------------------------

function modifier_directional_move:OnCreated( kv )
	if IsServer() then
		local caster = self:GetParent()
		
		self.currForward = caster:GetForwardVector()
		self.newForward = caster:GetForwardVector()
		self.speed = caster:GetBaseMoveSpeed()

		self.flag = 0

		if self:ApplyHorizontalMotionController() == false then 
			self:Destroy()
			return
		end

		self:OnIntervalThink()
		self:StartIntervalThink( 1 )

	end
end


function modifier_directional_move:OnIntervalThink()
	--self.oldForward = self.newForward

	--print(self.flag)
	--self.flag = self.flag - 1
	--if self.flag < 0 then
	--	self.flag = 0
	--end

	--if self.flag == 0 then
		--self.newForward = self:GetParent():GetForwardVector()
		self.currForward = self:GetParent():GetForwardVector()
	--end

end
--------------------------------------------------------------------------------

function modifier_directional_move:OnDestroy()
	if IsServer() then
		self:GetParent():RemoveHorizontalMotionController( self )
		self:GetParent():Interrupt()
	end
end


--------------------------------------------------------------------------------

function modifier_directional_move:UpdateHorizontalMotion( me, dt )
	if IsServer() then

--[[
		if (self.newForward.x + 0.2) > self.currForward.x and self.currForward.x > (self.newForward.x - 0.2)  then


		--if self.newForward == self.currForward then
			self.speed = self.speed + 5
		else
			self.speed = self.speed - 5
		end

		if self.speed <= 0 then
			self.speed = 0
			self.currForward = self.newForward 
		end

		if self.speed >= 400 then
			self.speed = 400
		end
]]

		local vNewPos = self:GetParent():GetOrigin() + ( self.currForward * self.speed * dt )
		if not GridNav:CanFindPath( self:GetParent():GetOrigin(), vNewPos ) then
			--self:Destroy()
			--return
			--self.flag = 10
			--self.currForward = self.currForward * (-1)

			self:GetParent():SetForwardVector(self.currForward * (-1)) 
			vNewPos = self:GetParent():GetOrigin() + ( self.currForward * (-1) * self.speed * dt )
			me:SetOrigin( vNewPos )
		else
			me:SetOrigin( vNewPos )
		end
		

	end
end

--------------------------------------------------------------------------------

function modifier_directional_move:OnHorizontalMotionInterrupted()
	if IsServer() then
		self:Destroy()
	end
end

--------------------------------------------------------------------------------

function modifier_directional_move:DeclareFunctions()
	local funcs = 
	{
		MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS,
		MODIFIER_PROPERTY_TURN_RATE_PERCENTAGE,
	}
	return funcs
end

--------------------------------------------------------------------------------

function modifier_directional_move:GetModifierTurnRate_Percentage( params )
	return 0
end


