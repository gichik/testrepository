modifier_glide_move = class({})

--[[
Модификатор насильно смещает героя игрока (Parent) с постепенно увеличивающейся скоростью (speed)
в направлении (new_forward) точки, которую игрок указывает на карте мышкой.

Если в процессе смещения героя, игрок указывает новую точку, то скорость сдвига постепенно
уменьшается вплоть до нуля, после чего направление смещения меняется в сторону новой точки,
а скорость вновь постепенно нарастает. Таким образом создается эффект "скольжения по льду".

Направление расчитывается в отдельном скрипте-модификаторе "modifier_move_order",
и заносится в переменную "new_forward", что хранится в герое.

Без скрипте-модификатора "modifier_move_order" переменная "new_forward" всегда будет
одного и того же значения, из чего следует, что данный модификатор не будет менять
направление смещения, постоянно сдвигая героя лишь в том направлении,
в которое он смотрел при наложении данного модификатора.
]]



--------------------------------------------------------------------------------

--[[
Декларация используемых/изменяемых свойств
MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS - отвечает за смещение
MODIFIER_PROPERTY_TURN_RATE_PERCENTAGE - отвечает за скорость поврота героя
в сторону клика игрока
]]

function modifier_glide_move:DeclareFunctions()
	local funcs =
	{
		MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS,
		MODIFIER_PROPERTY_TURN_RATE_PERCENTAGE,
	}
	return funcs
end

--------------------------------------------------------------------------------
--[[ Скрываем данный модификатор от глаз игрока ]]

function modifier_glide_move:IsHidden()
	return false
end

--------------------------------------------------------------------------------
--[[ На всякий случай убираем возможность к рассеиванию данного модификатора ]]

function modifier_glide_move:IsPurgable()
	return false
end

--------------------------------------------------------------------------------
--[[
OnCreated - первый метод, что отрабатывает при навешивании данного модификатора
на игрока. Здесь мы объявляем необходимые переменные и заносим в них начальные значения.

parent - герой игрока;
new_forward - новое/последнее направление смещения
currForward - текущее направление смещения
speed - скорость смещения
tick - переменная-счетчик, обеспечивающая псевдо-задержки в некоторых случаях
beCollision - булевская переменная, обеспечивающая отключение возможности солкновения
между героями в некоторые моменты, чтобы они не застревали друг в друге

]]

function modifier_glide_move:OnCreated( kv )
	if IsServer() then
		local parent = self:GetParent()
        parent.new_forward = parent:GetForwardVector()
		self.currForward = parent:GetForwardVector()
		self.speed = parent:GetBaseMoveSpeed()
        self.tick = 0
		self.beCollision = false


		--хрен знает, зачем это, но надо
		if self:ApplyHorizontalMotionController() == false then 
			self:Destroy()
			return
		end

		--self:OnIntervalThink()
		--self:StartIntervalThink( 0.1 )

	end
end
--[[
function modifier_glide_move:OnIntervalThink()

	local parent = self:GetParent()

	if parent.new_forward then
		self.newForward = parent.new_forward
		--parent:SetForwardVector(parent.new_forward)
	end

end]]

--------------------------------------------------------------------------------

--[[ UpdateHorizontalMotion - метод, что производит все расчеты и
смещает героя каждую микросекунду. Описание под методом. ]]

function modifier_glide_move:UpdateHorizontalMotion( me, dt )
	if IsServer() then
        local parent = self:GetParent()

		if (parent.new_forward.x + 0.5) > self.currForward.x and self.currForward.x > (parent.new_forward.x - 0.5)  then
		--if parent.new_forward == self.currForward then
			--parent:SetForwardVector(parent.new_forward)
			self.currForward = parent.new_forward
			self.speed = self.speed + 5
		else
			self.speed = self.speed - 5
		end

		if self.speed <= 0 then
			self.speed = 0
			self.currForward = parent.new_forward
            --parent:SetForwardVector(parent.new_forward)
		end

		if self.speed >= 500 then
			self.speed = 500
		end

        local vNewPos = parent:GetOrigin() + ( self.currForward * self.speed * dt )

        local units = FindUnitsInRadius(parent:GetTeamNumber(), parent:GetOrigin(), nil, 70,
        DOTA_UNIT_TARGET_TEAM_ENEMY + DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_ALL,  0, 0, false)

        if self.tick >= 10 then
            if #units > 1 then
                self.beCollision = true
                self.tick = 0
            end
        else
            self.tick = self.tick + 1
        end


		if not GridNav:CanFindPath( parent:GetOrigin(), vNewPos ) then
			self.currForward = self.currForward * (-1)
			vNewPos = parent:GetOrigin() + ( self.currForward * self.speed * dt )
        elseif self.beCollision then
            self.currForward = self.currForward * (-1)
            vNewPos = parent:GetOrigin() + ( self.currForward * self.speed * dt )
            self.beCollision = false
		end

		me:SetOrigin( vNewPos )

	end
end

--[[
Сначала метод псравнивает текущее смещение с будущим (новым) и в зависимсоти от результата либо
увеличивает скорость смещения (если направления совпадают),
либо её уменьшает (если направления сильно разняться).

Если скорость сдвига доходит до нуля или определенного порога замедления,
то меняется направление движения с текущего на новое.

Если скорость сдвига переваливает за  верхний порог, то она выставляется
в ограничительный максимум.

Далее происходит расчет новой позиции героя "vNewPos"(куда его нужно сдвинуть);
Определяется, есть ли на его пути другие герои и если есть;

Далее происходит проверка на "свободу намеченной позиции".
Если к намеченной позиции можно пройти и там нет героев, то происходит смещение.
Если же к намеченной позиции нельзя пройти, или там есть герои, то
направление смещения меняется на противоположное.

Из-за возможного столкновения с героями, в данном варианте приходится использовтаь
флаг  "beCollision" и счетчик тиков "tick". Если от них отказаться, то герои могут
застрять друг в друге, так как вражеский герой будет несколько раз попадать в проверку
столкновения и направление движения у обоих будет постоянно меняться на противоположное
и обратно. Иными словами, они будут топтаться на месте.
"tick" позволяет один раз определить столкновение с помощью "beCollision"
и затем несколько тиков метод не обращать внимание на вражеского героя,
чтобы направление движения изменилось только один раз.
]]

--------------------------------------------------------------------------------
--[[Здесь происходит переопределение скорости вращения героя]]

function modifier_glide_move:GetModifierTurnRate_Percentage( params )
	return 0
end

--------------------------------------------------------------------------------
--[[Когда "неведомые силы" прерывают работу модификатора, он уничтожается]]

function modifier_glide_move:OnHorizontalMotionInterrupted()
	if IsServer() then
		--self:Destroy()
	end
end

--------------------------------------------------------------------------------
--[[Запускается, когда модификатор уничтожается]]


function modifier_glide_move:OnDestroy()
	if IsServer() then
		self:GetParent():RemoveHorizontalMotionController( self )
		self:GetParent():Interrupt()
	end
end
