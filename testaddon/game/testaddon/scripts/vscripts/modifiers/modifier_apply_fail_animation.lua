modifier_apply_fail_animation = class({})

--------------------------------------------------------------------------------

function modifier_apply_fail_animation:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_apply_fail_animation:IsPurgable()
	return false
end

--------------------------------------------------------------------------------

function modifier_apply_fail_animation:OnCreated( data )
	if IsServer() then
		self.tick = 1
		self:OnIntervalThink()
		self:StartIntervalThink( 0.1 )
	end
end


function modifier_apply_fail_animation:OnIntervalThink()

	if not self:GetParent():HasModifier("modifier_fail_animation") then
		self.tick = self.tick + 1
	end

	if self.tick >= 3 then
		self:GetParent():RemoveModifierByName("modifier_fail_animation")
		self:GetParent():AddNewModifier( self:GetParent(), nil, "modifier_fail_animation", {} )
		self.tick = 1
	end
end
--------------------------------------------------------------------------------

function modifier_apply_fail_animation:OnDestroy()
	if IsServer() then
		self:GetParent():RemoveModifierByName("modifier_fail_animation")
	end
end