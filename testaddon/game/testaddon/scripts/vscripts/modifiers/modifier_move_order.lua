modifier_move_order = class({})

--------------------------------------------------------------------------------

function modifier_move_order:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_move_order:IsPurgable()
	return false
end

--------------------------------------------------------------------------------

function modifier_move_order:OnCreated( kv )
	if IsServer() then
	end
end


function modifier_move_order:OnDestroy()

end

--------------------------------------------------------------------------------

function modifier_move_order:DeclareFunctions()
	local funcs = 
	{
		MODIFIER_EVENT_ON_ORDER,
	}
	return funcs
end

--------------------------------------------------------------------------------

function modifier_move_order:OnOrder(data)
    if not IsServer() then
    	

    end
	local caster = self:GetParent()
    local forward = ( data.new_pos -  caster:GetOrigin() ):Normalized()
	caster.new_forward = forward
	--print(forward)
	--caster:SetForwardVector(forward)
	--caster.newForward = forward
end




