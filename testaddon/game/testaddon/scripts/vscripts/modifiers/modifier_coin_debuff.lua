modifier_coin_debuff = class({})

--------------------------------------------------------------------------------

function modifier_coin_debuff:IsHidden()
	return false
end

function modifier_coin_debuff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
	}
end

function modifier_coin_debuff:GetTexture()
	return "modifier_coin_debuff"
end

function modifier_coin_debuff:GetModifierMoveSpeedBonus_Percentage()
	return self:GetStackCount()*(-5)
end

function modifier_coin_debuff:RemoveOnDeath()
	return true
end



