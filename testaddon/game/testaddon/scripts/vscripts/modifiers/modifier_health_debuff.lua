modifier_health_debuff = class({})

--------------------------------------------------------------------------------

function modifier_health_debuff:IsHidden()
	return true
end

function modifier_health_debuff:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_EXTRA_HEALTH_PERCENTAGE
	}
end

function modifier_health_debuff:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_health_debuff:GetTexture()
	return "modifier_coin_debuff"
end

function modifier_health_debuff:GetModifierExtraHealthPercentage()
	return -0.05
end

function modifier_health_debuff:RemoveOnDeath()
	return true
end
