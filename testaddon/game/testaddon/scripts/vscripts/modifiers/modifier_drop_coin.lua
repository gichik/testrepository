modifier_drop_coin = class({})


function modifier_drop_coin:DeclareFunctions()
	local funcs = { MODIFIER_EVENT_ON_ATTACKED }
	return funcs
end

function modifier_drop_coin:OnCreated(data)
	if IsServer() then
		self.MaxAttacks = data.max_attack
		self.AttackCount = 0
		print(self.MaxAttacks)
	end
end

function modifier_drop_coin:OnAttacked(data)
	if IsServer() then
		if data.target:HasModifier("modifier_drop_coin")  then
			self.AttackCount = self.AttackCount + 1
			if self.AttackCount >= self.MaxAttacks then
				self.AttackCount = 0
				local name = "item_pickup_coin_" .. RandomInt(1,3)
				CreateCoin(name, self:GetParent():GetAbsOrigin())
			end
		end
	end
end


function modifier_drop_coin:OnDestroy()
	if IsServer() then
	end
end


function modifier_drop_coin:RemoveOnDeath()
	return false
end

function modifier_drop_coin:IsHidden()
	return false
end


function ApplyModifier(data)
	local attack_count = data.ability:GetSpecialValueFor("attack_count")
	print("special: " .. attack_count)
	print("AttackForDrop: " .. data.AttackForDrop)
	data.caster:AddNewModifier( caster, nil, "modifier_drop_coin", { max_attack = attack_count} )
end

function CreateCoin(itemName, pos)
	local newItem = CreateItem(itemName, nil, nil)
	newItem:SetPurchaseTime(0)
	CreateItemOnPositionSync(pos, newItem)
	newItem:LaunchLoot(false, 300, 0.75, pos + RandomVector(RandomFloat(100, 350)))
end
