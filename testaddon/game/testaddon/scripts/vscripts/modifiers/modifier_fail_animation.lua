modifier_fail_animation = class({})

--------------------------------------------------------------------------------

function modifier_fail_animation:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_fail_animation:IsPurgable()
	return false
end

--------------------------------------------------------------------------------

function modifier_fail_animation:OnCreated( data )
	if IsServer() then
	end
end

function modifier_fail_animation:DeclareFunctions()
	local funcs = 
	{
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}
	return funcs
end

--------------------------------------------------------------------------------

function modifier_fail_animation:GetOverrideAnimation( data )
	return ACT_DOTA_FLAIL
end
